![](docs/logo.png)

This repository contains runscripts for the EUREC4A ICON experiments. Analysis and planning of new experiments is done in the [warm_eurec4a](https://gitlab.gwdg.de/hernan.campos/warm_eurec4a) project.

If you are interested in the data the [how to eurec4a website](https://howto.eurec4a.eu/icon_les.html) is a good starting point. Also feel free to contact me.


