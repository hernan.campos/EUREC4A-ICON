Useful commits
==============

- 998ecc02de11f29e32dcb3adfe30e85185463576
  single test domain, reduced radiation grid, sstmode == 1, no RTTOV
- 7935123e815de656e21147dab080782517ab42aa
  single test domain, reduced radiation grid, sstmode == 6, no RTTOV
- 86ac42f09556b714bba93731e5392d3d0581145f
  singel test domain, reduced radiation grid, sstmode == 6, smooth sst field, z_ifc output, no RTTOV
- 6aec5445d386160edae270051b23d3406e6c6378 (exp: 28584271)
  single test domain, reduced radiation grid, sstmode == 6, no RTTOV, python prepared latbc and initc
- 12174f816fc1fb82f5b7c7067b5d8ee484095939 (exp: 28778196)
  single test domain, reduced radiation grid; sstmode ==6, with RTTOV (full grid)
