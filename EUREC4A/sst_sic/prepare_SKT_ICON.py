"""Script to convert OSTIA SST as ICON input

- Rename variables
- Add pseudo sea ice fraction ( all zeros)
- Remove skin temperatures that are not completely ocean gridpoints
"""

import xarray as xr
import numpy as np
import datetime as dt

input_fn = "./data/ERA5_SSTs_uv.nc"
mask_fn = "./data/era5_landsea_mask.nc"
ds_in = xr.open_dataset(input_fn)
ds_mask = xr.open_dataset(mask_fn)

ds_out = ds_in.copy()
ds_out = ds_out.rename({"skt": "SST"})  # variable needs to be named 'SST' to be recognised by ICON
del ds_out['sst']
del ds_out['u10']
del ds_out['v10']

print(ds_out.SST.where(ds_mask.squeeze().lsm == 0))

ds_out['SST'] = ds_out.SST.where(ds_mask.squeeze().lsm == 0)


ds_out['SIC'] = xr.DataArray(np.zeros(np.shape(ds_out.SST)), dims=['time','latitude','longitude'])
ds_out.SIC.attrs = {"units":"1", "long_name":"sea_ice_are_fraction", "description": "just field of zeros, since region of interest does not contain ice"}

ds_out['latitude'].attrs['units'] = "degrees_north"
ds_out['latitude'].attrs['standard_name'] = "latitude"
ds_out['longitude'].attrs['units'] = "degrees_east"
ds_out['longitude'].attrs['standard_name'] = "longitude"

current_time = dt.datetime.now().strftime('%d.%m.%Y %H:%M')

ds_out.attrs['history'] = f'Adapted by {__file__} on {current_time};'+ds_out.attrs['history']

ds_out.to_netcdf("./data/sst_sic.nc", encoding={"SST":{"dtype":"float32"},"SIC":{"dtype":"int8","_FillValue":-999, "zlib":True}})
