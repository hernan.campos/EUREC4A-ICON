"""Script to convert OSTIA SST as ICON input

- Rename variables
- Add pseudo sea ice fraction ( all zeros)
- Convert SST to Kelvin
"""

import xarray as xr
import numpy as np
import datetime as dt

input_fn = "./data/jplUKMO_OSTIAv20.nc"
ds_in = xr.open_dataset(input_fn)

ds_out = ds_in.copy()
ds_out = ds_out.rename({"analysed_sst": "SST"})  # variable needs to be named 'SST' to be recognised by ICON


# Convert SST to K
if np.any(ds_out.SST < 100):
    print('SST seems to be Celsius. Will be converted to Kelvin')
    ds_out.SST.data += 273.15  # Convert to Kelvin
    ds_out.SST.attrs['units'] = 'K'
    ds_out.SST.attrs['valid_max'] = ds_out.SST.attrs['valid_max']+273.15
    ds_out.SST.attrs['valid_min'] = ds_out.SST.attrs['valid_min']+273.15
    ds_out.SST.attrs['colorBarMaximum'] = ds_out.SST.attrs['colorBarMaximum']+273.15
    ds_out.SST.attrs['colorBarMinimum'] = ds_out.SST.attrs['colorBarMinimum']+273.15

ds_out['SIC'] = xr.DataArray(np.zeros(np.shape(ds_out.SST)), dims=['time','latitude','longitude'])
ds_out.SIC.attrs = {"units":"1", "long_name":"sea_ice_are_fraction", "description": "just field of zeros, since region of interest does not contain ice"}

current_time = dt.datetime.now().strftime('%d.%m.%Y %H:%M')

ds_out.attrs['history'] = f'Adapted by prepare_SST_ICON.py on {current_time};'+ds_out.attrs['history']

ds_out.to_netcdf("./data/sst-sic.nc")
