#!/bin/bash
# Regridding data onto regular lat lon grid
#SBATCH --account=mh0010
#SBATCH --job-name=latlon_postprocessing
#SBATCH --partition=compute2,compute
#SBATCH --nodes=5
#SBATCH --threads-per-core=2
#SBATCH --output=/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/postprocessing/regridding/logs/LOG.regrid_latlon.%j.o
#SBATCH --error=/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/postprocessing/regridding/logs/LOG.regrid_latlon.%j.o
#SBATCH --exclusive
#SBATCH --time=00:20:00
#SBATCH --mail-user=hauke.schulz@mpimet.mpg.de
#SBATCH --mail-type=ALL

experiment=$1
input=$2
mkdir -p ../../experiments/${experiment}/latlon/

cdo --version
git describe --always --dirty

cd ../../experiments/${experiment}/
echo $PWD

# Create weights for weighted remapping
if ! [ -f "../../grids/EUREC4A_PR1250m_DOM01_weights.nc" ]; then
  echo "Create weights for DOM01"
  cdo gendis,../../grids/lat-lon_DOM01.gridspec -selname,cell_area ../../grids/EUREC4A_PR1250m_DOM01.nc ../../grids/EUREC4A_PR1250m_DOM01_weights.nc
fi
if ! [ -f "../../grids/EUREC4A_PR1250m_DOM02_weights.nc" ]; then
  echo "Create weights for DOM02"
  cdo gendis,../../grids/lat-lon_DOM02.gridspec -selname,cell_area ../../grids/EUREC4A_PR1250m_DOM02.nc ../../grids/EUREC4A_PR1250m_DOM02_weights.nc
fi

if [[ $input == *DOM01* ]]; then
  echo Regridding file $input
  #cdo -r -P 10 -f nc4 -z zip remap,../../grids/lat-lon_DOM01.gridspec,../../grids/EUREC4A_PR1250m_DOM01_weights.nc ${input} latlon/${input::${#input}-3}_latlon.nc
  
  cdo -r -P 13 setgrid,../../grids/EUREC4A_PR1250m_DOM01_CellArea.nc ${input} tmp_setgrid_01_${SLURM_JOB_ID}.nc
  cdo -r -P 13 -f nc4 -z zip -remapdis,../../grids/lat-lon_DOM01.gridspec tmp_setgrid_01_${SLURM_JOB_ID}.nc /scratch/m/m300408/EUREC4A_Simulation/latlon/${input::${#input}-3}_latlon.nc
  rm tmp_setgrid_01_${SLURM_JOB_ID}.nc
  md5sum $input | cut -d " " -f 1 >> /work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/postprocessing/regridding/file_hashs.txt
  echo File written to latlon/${input::${#input}-3}_latlon.nc
fi

if [[ $input == *DOM02* ]]; then
  echo Regridding file $input
  #cdo -r -P 10 -f nc4 -z zip setgrid,../../grids/EUREC4A_PR1250m_DOM02_CellArea.nc -remap,../../grids/lat-lon_DOM02.gridspec,../../grids/EUREC4A_PR1250m_DOM02_weights.nc ${input} latlon/${input::${#input}-3}_latlon.nc
  cdo -r -P 13 setgrid,../../grids/EUREC4A_PR1250m_DOM02_CellArea.nc ${input} tmp_setgrid_02_${SLURM_JOB_ID}.nc
  cdo -r -P 13 -f nc4 -z zip -remapdis,../../grids/lat-lon_DOM01_0.00625deg.gridspec tmp_setgrid_02_${SLURM_JOB_ID}.nc /scratch/m/m300408/EUREC4A_Simulation/latlon/${input::${#input}-3}_latlon.nc
  rm tmp_setgrid_02_${SLURM_JOB_ID}.nc
  md5sum $input | cut -d " " -f 1 >> /work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/postprocessing/regridding/file_hashs.txt
  echo File written to latlon/${input::${#input}-3}_latlon.nc
fi

