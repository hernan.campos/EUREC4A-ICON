#!/bin/bash
# Regridding data onto regular lat lon grid
#SBATCH --account=mh0010
#SBATCH --job-name=latlon_postprocessing
#SBATCH --partition=shared
#SBATCH --nodes=1
#SBATCH --threads-per-core=2
#SBATCH --output=logs/LOG.regrid_latlon.%j.o
#SBATCH --error=logs/LOG.regrid_latlon.%j.o
#SBATCH --time=04:00:00
#SBATCH --mail-user=hauke.schulz@mpimet.mpg.de
#SBATCH --mail-type=ALL

experiment=$1
input_pattern=$2
mkdir -p ../../experiments/${experiment}/latlon/

cdo --version
git describe --always --dirty

cd ../../experiments/${experiment}/
echo $PWD

input_files=`ls ${input_pattern}`

touch /work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/postprocessing/regridding/file_hashs.txt

for file in ${input_files}
do
  echo "Checking ${file} for remapping"
  input_md5=`md5sum ${file} | cut -d " " -f 1`
  if grep -Fxq ${input_md5} /work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/postprocessing/regridding/file_hashs.txt
  then
    echo "File ($file) already regridded"
  else
    sbatch /work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/postprocessing/regridding/regrid_unstruct2latlon_singlefile.sh ${experiment} ${file}
  fi
done

