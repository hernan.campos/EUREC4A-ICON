#!/bin/bash
#SBATCH --account=mh0010
#SBATCH --job-name=compress
#SBATCH --partition=compute2,compute,prepost
#SBATCH --chdir=/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/experiments/EUREC4A
#SBATCH --nodes=1
#SBATCH --threads-per-core=2
#SBATCH --output=/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/run/logs/LOG.compress_reff.%j.o
#SBATCH --error=/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/run/logs/LOG.compress_reff.%j.o
#SBATCH --exclusive
#SBATCH --time=08:00:00
#SBATCH --mail-user=hauke.schulz@mpimet.mpg.de
#SBATCH --mail-type=ALL
#=============================================================================

for file in `ls *reff*.nc`
do
 echo $file
 nccopy -u -d5 -s $file compressed/$file
done
