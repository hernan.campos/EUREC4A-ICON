#!/bin/ksh
#=============================================================================
# =====================================
# mistral batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=mh0492
#SBATCH --job-name=exp.DOM01smallRTTOV.run
#SBATCH --partition=compute2,compute
#SBATCH --nodes=15
#SBATCH --threads-per-core=2
#SBATCH --output=/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/dev/run/logs/LOG.exp.DOM01+02small.run.%j.o
#SBATCH --error=/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/dev/run/logs/LOG.exp.DOM01+02small.run.%j.o
#SBATCH --exclusive
#SBATCH --time=00:10:00
#SBATCH --mail-user=hauke.schulz@mpimet.mpg.de
#SBATCH --mail-type=ALL
#=============================================================================
git describe --always --dirty
set -x
. ./add_run_routines  # Includes helper functions like warning/print_linked_files/...

#-----------------------------------------------------------------------------
export F_NORCW=65535
ulimit -s unlimited
#-----------------------------------------------------------------------------
# absolute paths of directories
calldir=$(pwd)
thisdir=$(pwd)
BASE_DIR=${thisdir%/*}                   # determine base directory
DATA_DIR=/mnt/lustre02/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/dev/
ICON_BASE_PATH=/mnt/lustre02/work/mh0010/m300408/Simulations/ICON/icon-aes
export ICON_BASE_PATH
export MODEL=${ICON_BASE_PATH}/bin/icon
RUNSCRIPT_NAME=`basename $0`

date=20200215
start_date="2020-02-15T10:00:00Z"
start_date_DOM02="2020-02-15T10:01:00Z"
end_date="2020-02-15T10:10:00Z"
init_date=`echo ${start_date%%:*} | sed 's/\-//g' | sed 's/T//g'`
#-----------------------------------------------------------------------------
# target parameters
# ----------------------------
job_name="exp.DOM01small.run"
submit="sbatch "
#-----------------------------------------------------------------------------
# openmp environment variables
# ----------------------------

# OpenMP settings
export OMP_NUM_THREADS=6
export ICON_THREADS=1
export OMP_SCHEDULE=dynamic,1
export OMP_DYNAMIC="false"
export OMP_STACKSIZE=1024M


#### NEW MODIFICATIONS #####
export OMPI_MCA_pml=cm         # sets the point-to-point management
export OMPI_MCA_mtl=mxm        # sets the matching transport layer (MPI-2 one-sided comm)
export MXM_RDMA_PORTS=mlx5_0:1
export MXM_LOG_LEVEL=ERROR
export MXM_HANDLE_ERRORS=bt
export UCX_HANDLE_ERRORS=bt

# enable HCOLL based collectives
export OMPI_MCA_coll=^fca              # disable FCA for collective MPI routines
export OMPI_MCA_coll_hcoll_enable=1    # enable HCOLL for collective MPI routines
export OMPI_MCA_coll_hcoll_priority=95
export OMPI_MCA_coll_hcoll_np=8        # use HCOLL for all communications with more than 8 tasks
export HCOLL_MAIN_IB=mlx5_0:1
export HCOLL_ENABLE_MCAST=1
export HCOLL_ENABLE_MCAST_ALL=1

# disable specific HCOLL functions (strongly depends on the application)
export HCOLL_ML_DISABLE_BARRIER=1
export HCOLL_ML_DISABLE_IBARRIER=1
export HCOLL_ML_DISABLE_BCAST=1
export HCOLL_ML_DISABLE_REDUCE=1
#-----------------------------------------------------------------------------
# MPI variables
# ----------------------------
mpi_root=/opt/mpi/bullxmpi_mlx/1.2.9.2
no_of_nodes=${SLURM_JOB_NUM_NODES:=1}
mpi_procs_pernode=$((${SLURM_JOB_CPUS_PER_NODE%%\(*} /2/OMP_NUM_THREADS))
((mpi_total_procs=no_of_nodes * mpi_procs_pernode))
ulimit -s $((${OMP_STACKSIZE/M/ * 1024}))
START="srun --cpu-freq=HighM1 --kill-on-bad-exit=1 --nodes=${SLURM_JOB_NUM_NODES:-1} --cpu_bind=verbose,cores --distribution=block:block --ntasks=${mpi_total_procs} --ntasks-per-node=${mpi_procs_pernode} --cpus-per-task=$((2 * OMP_NUM_THREADS)) --propagate=STACK,CORE"

# determine architecture
arch=x86_64-unknown-linux-gnu

#if [ -a  /etc/profile ] ; then
#. /etc/profile
#module purge
# Debugging
#loadmodule="arm-forge/19.1.4 $loadmodule"
#module load arm-forge
#START="ddt --connect $START"
#fi
set +x
#-----------------------------------------------------------------------------
ICONDIR=${ICON_BASE_PATH}
RUNSCRIPTDIR=${ICONDIR}/run
export EXPNAME="EUREC4A_RTTOV"

# experiment directory
EXPDIR=${BASE_DIR}/experiments/${EXPNAME}
if [ ! -d ${EXPDIR} ] ;  then
  mkdir -p ${EXPDIR}
fi
check_error $? "${EXPDIR} does not exist?"
cd ${EXPDIR}

# -----------------------------------------------------------------------------
# check if this is a restart or not
restartSemaphoreFilename='isRestartRun.sem'             # name of dummy semaphore file
restart=.FALSE.                                         # restart variable is initially false
if [ -f ${BASE_DIR}/run/${restartSemaphoreFilename} ]; then             # if the file "isRestartRun.sem" exists, then set restart to true
  restart=.TRUE.
  previous_exp_folder=`cat ${BASE_DIR}/run/restart_location.txt`        # get previous experiment
  rm ${BASE_DIR}/run/multifile_restart_atm.mfr
  echo "Linking to restart file of last job"
  ln -s ${previous_exp_folder}/multifile_restart_atm.mfr ${BASE_DIR}/run/multifile_restart_atm.mfr
  echo "Linkage done"
fi
echo This is a restart: ${restart}
# -----------------------------------------------------------------------------
# LINKING FILES
# -----------------------------------------------------------------------------
# Radiation
ANCILLARY_DIR=/work/mh0010/m300408/Simulations/ICON/icon-aes/data
add_link_file ${ANCILLARY_DIR}/rrtmg_lw.nc                 ./
add_link_file ${ANCILLARY_DIR}/ECHAM6_CldOptProps.nc       ./
add_link_file ${ANCILLARY_DIR}/dmin_wetgrowth_lookup.nc    ./

# Grids
LATBC_GRID=${DATA_DIR}/latbc/lateral_boundary_DOM01.grid.nc
GRID_DIR=${DATA_DIR}/grids
add_link_file ${GRID_DIR}/Testdomain_land_DOM01_DOM01.nc    ./grid_dynamics_1.nc
add_link_file ${GRID_DIR}/Testdomain_land_DOM01_DOM02.nc    ./grid_dynamics_2.nc

# Initial data
INIT_DIR=${DATA_DIR}/initc/${date}
add_link_file ${INIT_DIR}/initc_Testdomain_land_DOM01_DOM01_${init_date}.nc dwdFG_R2B12_DOM01.nc

# Lateral boundary conditions
LATBC_PATH=${DATA_DIR}/latbc
LATBC_FILENAME='<y><m><d>/latbc_Testdomain_land_DOM01_DOM01_<y><m><d><h>.nc'

# Sea surface temperature/ Sea ice cover conditions
SSTSIC_PATH=${DATA_DIR}/sst_sic/data
add_link_file ${SSTSIC_PATH}/sst_sic_DOM01.nc ./SSTSIC_grid_dynamics_1.nc
add_link_file ${SSTSIC_PATH}/sst_sic_DOM02.nc ./SSTSIC_grid_dynamics_2.nc

# EXTPAR
EXTPAR_PATH=${DATA_DIR}/extpar
add_link_file ${EXTPAR_PATH}/external_parameter_icon_Testdomain_land_DOM01_DOM01_tiles.nc  extpar_grid_dynamics_1.nc
add_link_file ${EXTPAR_PATH}/external_parameter_icon_Testdomain_land_DOM01_DOM02_tiles.nc  extpar_grid_dynamics_2.nc


# add RTTOV coefficient files
echo "add RTTOV coefficient files"
add_required_file ${BASE_DIR}/data/rttov/const/rt13coeffs_rttov7pred54L/rtcoef_msg_2_seviri.dat .
add_required_file ${BASE_DIR}/data/rttov/const/rt13coeffs_rttov7pred54L/sccldcoef_msg_2_seviri.dat .

# fligh track files
echo "add flight track files"
add_required_file /work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/dev/run/flight_track.dat .
add_required_file /work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/dev/run/platform_track_Meteor_init202002151000.dat .
add_required_file /work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/dev/run/flight_track_subset.dat .
add_required_file ${ICON_BASE_PATH}/data/HALO_thinned_20131211.dat .


# -----------------------------------------------------------------------------
# NAMELISTS
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# global namelist settings

# the namelist filename
atmo_namelist=NAMELIST_${EXPNAME}

# global timing
ndays_restart=0.0007
dt_restart=90  #`expr ${ndays_restart} \* 86400`  # output of checkpoint and restart
ndays_checkpoint=0.0007
dt_checkpoint=90 #`expr ${ndays_restart} \* 86400`  # output of checkpoint only

# relative start times of nests and meteograms
start_times="0.,12."

# the grid parameters
dynamics_grid_filenames="grid_dynamics_1.nc,grid_dynamics_2.nc"
dynamics_parent_grid_ids="0,1"
#radiation_grid_filenames="grid_radiation_1.nc"  # only for coarsest grid

# -----------------------------------------------------------------------------
# ICON master namelist
cat > icon_master.namelist << EOF
&master_nml
 lrestart                     = ${restart}
 lrestart_write_last          = .true.
/
&master_time_control_nml
 experimentStartDate = "$start_date"
 experimentStopDate  = "$end_date"
 calendar            = 1
 !restartTimeIntVal   = "PT01M"
 !checkpointTimeIntVal = "PT30S"
/
&time_nml
 ini_datetime_string          = "$start_date"
 dt_restart                   = $dt_restart  ! checkpoints are defined in io_nml
/
&master_model_nml
  model_type                  = 1
  model_name                  = "ATMO"
  model_namelist_filename     = "$atmo_namelist"
  model_min_rank              = 1
  model_max_rank              = 65536
  model_inc_rank              = 1
/
EOF

# -----------------------------------------------------------------------------
# ICON model namelist

cat > ${atmo_namelist} << EOF
!
&parallel_nml
 nproma         = 16
 p_test_run     = .false.
 l_test_openmp  = .true.  ! only effective if p_test_run == .true.
 l_log_checks   = .false.
 num_io_procs   = 4  ! FESSTVAL:1; NARVAL:5; HALO:default
 num_prefetch_proc = 1
/
&grid_nml
 ! cell_type is not used = 3            ! triangular cells
 dynamics_grid_filename  = ${dynamics_grid_filenames}
 dynamics_parent_grid_id = ${dynamics_parent_grid_ids}
 !radiation_grid_filename = ${radiation_grid_filenames}
 lfeedback               = .false.
 l_limited_area          = .true.
 start_time              = ${start_times}
 lredgrid_phys           = .false.
/
&initicon_nml
 init_mode              =  7 !4=cosmo, 2=ifs, 3=combined
 dwdfg_filename         = 'dwdFG_R2B12_DOM<idom>.nc'
 lread_ana              = .false.
 ltile_init             = .true.
 ltile_coldstart        = .true.
/
&limarea_nml
 itype_latbc     = 1
 dtime_latbc     = 3600.  ! Time difference between latbc forcing (seconds)
 latbc_boundary_grid = '${LATBC_GRID}'
 latbc_path      = '${LATBC_PATH}'
 latbc_filename  = '${LATBC_FILENAME}'
 init_latbc_from_fg = .true.
/
&io_nml
 itype_pres_msl         = 5
 dt_checkpoint          = ${dt_checkpoint}
 lmask_boundary         = .true.
 lkeep_in_sync          = .false.
 restart_file_type      = 5
 restart_write_mode     = 'joint procs multifile'
/
&gribout_nml
 lgribout_compress_ccsds = .TRUE.
/
&run_nml
 num_lev     = 50
 lvert_nest  = .false.
 dtime       = 6            ! why only one value and not one for each nest?
 ldynamics   = .TRUE.       ! dynamics
 ltestcase   = .FALSE.      ! testcase
 ltransport  = .TRUE.       ! transport (but ntracer is 0 by default)
 ntracer     = 5
 iforcing    = 3            ! 0: no forcing (default), 3: NWP forcing
 ltimer      = .true.       !
 timers_level = 10          !
 profiling_output = 1       !
 msg_level   = 9            ! detailed report during integration
 output      = 'nml','totint'
 check_uuid_gracefully = .True.
/
&dynamics_nml
 iequations  = 3
 lcoriolis   = .TRUE.
 idiv_method     = 1
 divavg_cntrwgt  = 0.50
/
&diffusion_nml
 hdiff_order      = 5
 lhdiff_vn        = .TRUE.
 lhdiff_temp      = .TRUE.
 lhdiff_w         = .TRUE.
 hdiff_efdt_ratio = 24       ! recommendation is 30, default is 36
 hdiff_smag_fac   = 0.025    ! default is 0.015
 lsmag_3d         = .FALSE.  ! recommended for mesh size < 1km IF inwp_turb != 5 scheme not used
/
&nwp_phy_nml
inwp_gscp       = 4,4        ! 4 for two moment
mu_rain         = 0.5        ! shape parameter in gamma distribution for rain
inwp_convection = 0,0
inwp_radiation  = 1,1
inwp_cldcover   = 5,5        ! 1: diagnostic cloud cover (FESSTVAL); 5: grid scale clouds (NARVAL, HALO)
inwp_turb       = 5,5
inwp_satad      = 1,1
inwp_sso        = 0,0        ! subgrid scale orographic drag 0: none, 1: Lott and Miller scheme (default)
inwp_gwd        = 0,0        ! non-orographic gravity wave drag 0: none, 1: Orr-Ern-Bechtold-scheme (default)
inwp_surface    = 1,1        ! surface scheme 0: none, 1: TERRA (default)
latm_above_top  = .true.,.true.
efdt_min_raylfric = 7200.    ! minimum e-folding time of Rayleigh friction (effective for u > 250 m/s)
itype_z0         = 2
dt_rad           = 10.,10.  ! default: 1800s; radiation timestepping; must be multple of dt_conv
dt_gwd           = 5.,5.  ! default: 1200s
dt_sso           = 5.,5.  ! default: 1200s
dt_conv          = 5.,5.  ! default: 600s
!icalc_reff = 100!,100
!icpl_rad_reff = 1!,1
/
&les_nml
 isrfc_type        = 1  !1=TERRA,2=Fixed flux, 5=fixed SST, 3=fixed bflux
 ldiag_les_out     = .FALSE.
 les_metric        = .TRUE.
/
&lnd_nml
ntiles   = 1
lmulti_snow = .false.
lsnowtile = .false.
frlnd_thrhld = 0.5
frlake_thrhld = 0.5
frsea_thrhld = 0.5
lseaice   = .false.
llake     = .false.
itype_lndtbl   = 3
itype_heatcond = 3
sstice_mode = 1
sst_td_filename = "<path>SSTSIC_<gridfile>"
ci_td_filename = "<path>SSTSIC_<gridfile>"
/
&radiation_nml
 irad_o3 = 7
 irad_aero = 6
 vmr_co2   = 390.e-06  ! values representative for 2012
 vmr_ch4   = 1800.e-09
 vmr_n2o   = 322.0e-09
 vmr_o2    = 0.20946
 vmr_cfc11 = 240.e-12
 vmr_cfc12 = 532.e-12
 albedo_type = 2
/
&nonhydrostatic_nml
 iadv_rhotheta    =  2      ! default (2)
 ivctype          =  2      ! default (2)
 itime_scheme     =  4      ! default (4)
 exner_expol      =  0.333  ! default (1./3.)
 vwind_offctr     =  0.25   ! default (0.15); NARVAL: 0.25
 damp_height      =  15000. ! default (45000); NARVAL: 25000.; HALO: 15000; FESSTVAL: 12250
 rayleigh_coeff   =  0.25   ! default (0.05; higher values are recommended for R2B6 and finer)
 lhdiff_rcf       = .true.  ! default (true)
 divdamp_fac      =  0.004  ! default (0.0025); Scaling factor of divergence damping
 divdamp_order    =  4     ! default (4); 24: does not allow checkpointing/restarting earlier than 2.5h of integration
 divdamp_type     =  32     ! default (3); NARVAL: 32
 l_open_ubc       = .true.
 igradp_method    =  3      ! default (3)
 l_zdiffu_t       = .true.  ! default (true)
 thslp_zdiffu     =  0.02   ! default (0.025)
 thhgtd_zdiffu    =  125.   ! default (200)
 htop_moist_proc  =  15500. ! default (22500); NARVAL: 22500
 hbot_qvsubstep   =  15500. ! default (22500); NARVAL: 19000
/
&sleve_nml
 min_lay_thckn   = 20.     ! default (50); NARVAL, FESSTVAL, HALO: 20
 top_height      = 21000.  ! default (23500); NARVAL: 30000; FESSTVAL: 22000; HALO: 21000
 stretch_fac     = 0.9     ! default (1); NARVAL: 0.9; FESSTVAL: 0.65
 decay_scale_1   = 4000.   ! default (4000)
 decay_scale_2   = 2500.   ! default (2500)
 decay_exp       = 1.2     ! default (1.2)
 flat_height     = 16000.  ! default (16000)
/
!&turbdiff_nml !!! Daniel Dynmod setup
!  tkhmin                      = 0.75  !=MBOld=NARVAL
!  tkmmin                      = 0.75  !=MBOld=NARVAL
!  tkmmin_strat                = 4     ! NA in MBOld
!  pat_len                     = 750.  !=MBOld=NARVAL
!  c_diff                      = 0.2   !=MBOld=NARVAL
!  rat_sea                     = 7.0   !=8.0 in MBOld
!  ltkesso                     = .TRUE.!=MBOld=NARVAL
!  frcsmot                     = 0.2   !=MBOld! these 2 switches together apply vertical smoothing of the TKE source terms
!  imode_frcsmot               = 2     !=MBOld! in the tropics (only), which reduces the moist bias in the tropical lower troposphere
!                                      ! use horizontal shear production terms with 1/SQRT(Ri) scaling to prevent unwanted side effects:
!  itype_sher                  = 3     !=MBOld=NARVAL
!  ltkeshs                     = .TRUE.!=MBOld=NARVAL
!  a_hshr                      = 2.0   !=MBOld=NARVAL
!  alpha0                      = 0.0123!=MBOld=NARVAL
!  alpha0_max                  = 0.0335!=MBOld=NARVAL
!  icldm_turb                  = 1     !=NA in MBOld   !Mode of water cloud representation in turbulence
!/
&transport_nml
 ivadv_tracer      = 3,3,3,3,3,3
 itype_hlimit      = 3,4,4,4,4,4
 ihadv_tracer      = 52,2,2,2,2,2
 llsq_svd          = .true.
 beta_fct          = 1.005
/




! RTTOV BEGIN
!&synsat_nml
! lsynsat = .true.,
!/

!&synsat_model_nml
! rttov_coefficient_filename = '/work/mh1119/m300843/rttov/rttov_coef_downloads/rtcoef/rtcoef_goes_16_abi_7gas_ironly.dat'
! !'/work/mh1119/m300843/rttov/rttov13/rtcoef_rttov13/rttov13pred54L/rtcoef_goes_16_abi_7gas.dat'
! rttov_cloud_scattering_coefficient_filename = '/work/mh1119/m300843/rttov/rttov_coef_downloads/sccld/sccldcoef_goes_16_abi_ironly.dat'
! channels     = 1, 2, 3, 4, 5, 6, 7
! !channels     = 7, 8, 9, 10, 11, 12, 13
! patch_id     = 1
! run_start    = ${start_date}
! run_end      = ${end_date}
! run_interval = 'P00DT00H00M30S'
! satellite_subpoint_lat = 0.
! satellite_subpoint_lon = -75.2
! satellite_height = 35.786020e6
!/

!&output_nml
! filetype        = 4
! dom             = 1
! output_start    = ${start_date}
! output_end      = ${end_date}
! output_interval = 'P00DT00H00M30S'
! file_interval   = 'P00DT00H00M30S'
! output_filename = 'SYNSAT_RTTOV_FORWARD_MODEL'! output_grid     = .true.
! filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
! ml_varlist      = 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_1', 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_2', 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_3', 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_4', 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_5', 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_6', 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_7'
!/

!&synsat_model_nml
! rttov_coefficient_filename = '/work/mh1119/m300843/rttov/rttov_coef_downloads/rtcoef/rtcoef_goes_16_abi_7gas_ironly.dat'
! rttov_cloud_scattering_coefficient_filename = '/work/mh1119/m300843/rttov/rttov_coef_downloads/sccld/sccldcoef_goes_16_abi_ironly.dat'
! channels     = 1, 2, 3, 4, 5, 6, 7 ! originally 7, 8, 9, 10, 11, 12, 13
! patch_id     = 2
! run_start    = ${start_date_DOM02}
! run_end      = ${end_date}
! run_interval = 'P00DT00H00M30S'
! satellite_subpoint_lat = 0.
! satellite_subpoint_lon = -75.2
! satellite_height = 35.786020e6
!/

!&output_nml
! filetype        = 4
! dom             = 2
! output_start    = ${start_date_DOM02}
! output_end      = ${end_date}
! output_interval = 'P00DT00H00M30S'
! file_interval   = 'P00DT00H00M30S'
! output_filename = 'SYNSAT_RTTOV_FORWARD_MODEL'
! output_grid     = .true.
! ml_varlist      = 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_1', 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_2', 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_3', 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_4', 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_5', 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_6', 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_7'/
!/
!! ! RTTOV END





&gridref_nml
 grf_intmethod_e  = 5     ! default(6); NARVAL: 5
 grf_scalfbk      = 2     ! default(2)
 grf_tracfbk      = 2     ! default(2)
 denom_diffu_v    = 200.  ! default(200)
/
&interpol_nml
nudge_zone_width  = 10  !-1 create nudge zone in grid
lsq_high_ord      = 3  ! default (3)
!rbf_vec_scale_c   = 0.09, 0.030, 0.010, 0.0040
!rbf_vec_scale_v   = 0.21, 0.070, 0.025, 0.0025
!rbf_vec_scale_e   = 0.45, 0.150, 0.050, 0.0125
rbf_scale_mode_ll = 2 !2=default for automatic calculations
/
&extpar_nml
 itopo          = 1
 n_iter_smooth_topo = 1,1,1,1
 hgtdiff_max_smooth_topo = 750.,750.,750.,750.
 heightdiff_threshold = 1000.,1000.,750.,750.
 extpar_filename = '<path>extpar_<gridfile>'
/
&meteogram_output_nml
 lmeteogram_enabled= .TRUE.,.TRUE.,.TRUE.,.TRUE.
 n0_mtgrm          = ${start_times}           ! meteogram initial time step (0 is first step!)
 ninc_mtgrm        = 10,10,10,10              ! meteogram output interval (steps)
 max_time_stamps   = 10000,10000,10000,10000  ! number of output time steps to record in memory before flushing to disk
 zprefix           = 'Meteogram_${SLURM_JOB_ID}_DOM01_', 'Meteogram_${SLURM_JOB_ID}_DOM02_', 'Meteogram_${SLURM_JOB_ID}_DOM03_', 'Meteogram_${SLURM_JOB_ID}_DOM04_'
 append_if_exists  = .True.,.True.,.True.,.True.
 ldistributed      = .false,.false.,.false.,.false.
 stationlist_tot   =  13.3,  -57.717, 'c_enter',             ! Lat,Lon
                      12.3,  -57.717, 'c_south',
                      14.3,  -57.717, 'c_north',
                      13.3,  -56.717, 'c_east',
                      13.3,  -58.717, 'c_west',
                      13.16, -59.430, 'BCO'
 /

!&output_nml
! --------------------------------------------------------------------------- !
! ---  ICON-2D: output field test - FLIGHT TRACK                           --- !
! --------------------------------------------------------------------------- !
! filetype                     =  4                        ! output format: 2=GRIB2, 4=NETCDFv2
! dom                          =  -1                        ! write all domains
! output_time_unit             =  1                        ! 1: seconds
! output_bounds                =  0., 43200., 12.     ! start, end, increment
! !output_start                 = '2016-08-01T00:00:00Z'
! !output_end                   = '2016-08-01T22:00:00Z'
! !output_interval              = 'PT2M'
! steps_per_file               =  2
! mode                         =  1  ! 1: forecast mode (relative t-axis), 2: climate mode (absolute t-axis)
! include_last                 = .TRUE.
! output_filename              = 'meteor_DOM<physdom>'!'HALO_20131211' !           ! file name base
! ml_varlist                  = 'qv', 'temp', 'theta_v', 'qi', 'qr', 'qs', 'u', 'v', 'w', 'qc', 'clc', 'tke', 'z_mc', 'pres'
! output_grid                  = .TRUE.
! remap                        = 3                         ! 1: latlon,  0: native grid, 3: flight track
! flight_track_format          = 1 ! 1: plain ascii time in sec since model start; 2: HALO flight track file specific, time in sec since 00UTC
! flight_track_filename        = 'flight_track_subset.dat' !'HALO_thinned_20131211.dat'
!/


&output_nml  ! output of constant levels
 dom              = 1  ! master output_nml (-1)
 output_bounds    = 300.,300.,300.   ! adapt to start_times
 filetype         = 5
 steps_per_file   = 1
 include_last     = .true.
 mode             = 2                 ! (default: 2; absolute time axis; 1: relative time axis)
 output_filename  = 'constants'
 filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
 ml_varlist       = 'z_ifc','gz0','topography_c','fr_land','fr_lake',
 output_grid      = .TRUE.
/
&output_nml  ! output of constant levels
 dom              = 2  ! master output_nml (-1)
 output_bounds    = 300.,300.,300.   ! adapt to start_times
 filetype         = 5
 steps_per_file   = 1
 include_last     = .true.
 mode             = 2                 ! (default: 2; absolute time axis; 1: relative time axis)
 output_filename  = 'constants'
 filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
 ml_varlist       = 'z_ifc','gz0','topography_c','fr_land','fr_lake',
 output_grid      = .TRUE.
/
!&output_nml  ! output surface variables
! dom              = -1  ! master output_nml (-1)
! output_start     = "${start_date}"
! output_end       = "${end_date}"
! output_interval  = "PT00M30S"
! file_interval    = "PT00M30S"  !"P01D"
! include_last     = .FALSE.
! mode             = 2                 ! (default: 2; absolute time axis; 1: relative time axis)
! output_filename  = 'surface'
! filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
! ml_varlist       = 'u_10m','v_10m','rh_2m','t_2m','qv_2m','t_seasfc','shfl_s','lhfl_s','tqv_dia','tqc_dia','tqi_dia','rain_gsp_rate','tot_prec','clct','pres_sfc'
! output_grid      = .TRUE.
!/
!&output_nml  ! output radiation
! dom              = -1  ! master output_nml (-1)
! output_start     = "${start_date}"
! output_end       = "${end_date}"
! output_interval  = "PT00M30S"
! file_interval    = "PT00M30S"
!! include_last     = .FALSE.
! mode             = 2  ! (default: 2; absolute time axis; 1: relative time axis)
! output_filename  = '3D'
! filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
! ml_varlist       = 'u','v','w','div','temp','pres','theta_v','rho','qv','qc','qr','qi'
! output_grid      = .TRUE.
!/
&output_nml  ! output radiation
 dom              = -1  ! master output_nml (-1)
 filetype          = 5
 output_start     = "${start_date}"
 output_end       = "${end_date}"
 output_interval  = "PT00M30S"
 file_interval    = "PT00M30S"
! include_last     = .FALSE.
 mode             = 2  ! (default: 2; absolute time axis; 1: relative time axis)
 output_filename  = '3D'
 filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
 ml_varlist       = 'u','v','w','div','temp','pres','theta_v','rho','qv','qc','qr','qi'
 output_grid      = .TRUE.
/
!&output_nml  ! output radiation
! dom              = 1  ! master output_nml (-1)
! output_start     = "${start_date}"
! output_end       = "${end_date}"
! output_interval  = "PT00M30S"
!! file_interval    = "PT00M30S"
! include_last     = .FALSE.
! mode             = 2  ! (default: 2; absolute time axis; 1: relative time axis)
! output_filename  = 'radiation'
!!! filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
! ml_varlist       = 'sou_t','sob_t','sod_t','thb_t','ddt_temp_radsw','ddt_temp_radlw'  ! 'swflx_dn_clr','swflx_dn','swflx_up_clr','swflx_up','lwflx_dn_clr','lwflx_dn','lwflx_up_clr','lwflx_up',
! output_grid      = .TRUE.
!/
!&output_nml  ! output 3D variables
! dom              = 1  ! master output_nml (-1)
! output_start     = "${start_date}"
! output_end       = "${end_date}"
! output_interval  = "PT00M30S"
! file_interval    = "PT00M30S"
!! include_last     = .FALSE.
! mode             = 2  ! (default: 2; absolute time axis; 1: relative time axis)
! output_filename  = 'reff'
! filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
!! ml_varlist       = 'reff_qc','reff_qi'
! output_grid      = .TRUE.
!/
!&output_nml  ! output surface variables
! dom              = 2  ! master output_nml (-1)
! output_start     = "${start_date_DOM02}"
! output_end       = "${end_date}"
!! output_interval  = "PT00M30S"
! file_interval    = "PT00M30S"
! include_last     = .FALSE.
!! mode             = 2                 ! (default: 2; absolute time axis; 1: relative time axis)
! output_filename  = 'surface'
! filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
! ml_varlist       = 'u_10m','v_10m','rh_2m','t_2m','qv_2m','t_seasfc','shfl_s','lhfl_s','tqv_dia','tqc_dia','tqi_dia','rain_gsp_rate','tot_prec','clct','pres_sfc'
! output_grid      = .TRUE.
!/
!&output_nml  ! output radiation
! dom              = 2  ! master output_nml (-1)
! output_start     = "${start_date_DOM02}"
! output_end       = "${end_date}"
! output_interval  = "PT00M30S"
! file_interval    = "PT00M30S"
! include_last     = .FALSE.
!! mode             = 2  ! (default: 2; absolute time axis; 1: relative time axis)
! output_filename  = 'radiation'
! filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
!! ml_varlist       = 'sou_t','sob_t','sod_t','thb_t','ddt_temp_radsw','ddt_temp_radlw'  ! 'swflx_dn_clr','swflx_dn','swflx_up_clr','swflx_up','lwflx_dn_clr','lwflx_dn','lwflx_up_clr','lwflx_up',
! output_grid      = .TRUE.
!/
!&output_nml  ! output 3D variables
! dom              = 2  ! master output_nml (-1)
! output_start     = "${start_date_DOM02}"
! output_end       = "${end_date}"
! output_interval  = "PT00M30S"
! file_interval    = "PT00M30S"
!! include_last     = .FALSE.
! mode             = 2  ! (default: 2; absolute time axis; 1: relative time axis)
! output_filename  = 'reff'
! filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
!! ml_varlist       = 'reff_qc','reff_qi'
! output_grid      = .TRUE.
!/


EOF


#=============================================================================
#
# This section of the run script prepares and starts the model integration. 
#
# bindir and START must be defined as environment variables or
# they must be substituted with appropriate values.
#
# Marco Giorgetta, MPI-M, 2010-04-21
#
#-----------------------------------------------------------------------------
final_status_file=${ICON_BASE_PATH}/run/${job_name}.final_status
rm -f ${final_status_file}
#-----------------------------------------------------------------------------
final_status_file=${RUNSCRIPTDIR}/${job_name}.final_status
rm -f ${final_status_file}
#-----------------------------------------------------------------------------
# set some default values and derive some run parameteres
restart=${restart:=".false."}
restartSemaphoreFilename='isRestartRun.sem'
#AUTOMATIC_RESTART_SETUP:
if [ -f ${restartSemaphoreFilename} ]; then
  restart=.true.
  #  do not delete switch-file, to enable restart after unintended abort
  #[[ -f ${restartSemaphoreFilename} ]] && rm ${restartSemaphoreFilename}
fi
#END AUTOMATIC_RESTART_SETUP
#
# wait 5min to let GPFS finish the write operations
if [ "x$restart" != 'x.false.' -a "x$submit" != 'x' ]; then
  if [ x$(df -T ${EXPDIR} | cut -d ' ' -f 2) = gpfs ]; then
    sleep 10;
  fi
fi

#-----------------------------------------------------------------------------
# print_required_files
copy_required_files
link_required_files


#-----------------------------------------------------------------------------
# get restart files

if  [ x$restart_atmo_from != "x" ] ; then
  rm -f restart_atm_DOM01.nc
#  ln -s ${ICONDIR}/experiments/${restart_from_folder}/${restart_atmo_from} ${EXPDIR}/restart_atm_DOM01.nc
  cp ${ICONDIR}/experiments/${restart_from_folder}/${restart_atmo_from} cp_restart_atm.nc
  ln -s cp_restart_atm.nc restart_atm_DOM01.nc
  restart=".true."
fi
if  [ x$restart_ocean_from != "x" ] ; then
  rm -f restart_oce.nc
#  ln -s ${ICONDIR}/experiments/${restart_from_folder}/${restart_ocean_from} ${EXPDIR}/restart_oce.nc
  cp ${ICONDIR}/experiments/${restart_from_folder}/${restart_ocean_from} cp_restart_oce_DOM01.nc
  ln -s cp_restart_oce_DOM01.nc restart_oce_DOM01.nc
  restart=".true."
fi
#-----------------------------------------------------------------------------
read_restart_namelists=${read_restart_namelists:=".true."}
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#
#  get model
#
ls -l ${MODEL}
check_error $? "${MODEL} does not exist?"
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
# (re)start experiment

rm -f finish.status
date
${START} ${MODEL} # > out.txt 2>&1
date

if [ -r finish.status ] ; then
  check_final_status 0 "${START} ${MODEL}"    # ATTENTION: not defined!
else
  check_final_status -1 "${START} ${MODEL}"
fi
#
#-----------------------------------------------------------------------------
#
finish_status=`cat finish.status`
echo $finish_status
echo "============================"
echo "Script run successfully: $finish_status"
echo "============================"

#-----------------------------------------------------------------------------
namelist_list=""
#-----------------------------------------------------------------------------
# check if we have to restart, ie resubmit
#   Note: this is a different mechanism from checking the restart
if [ $finish_status = "RESTART" ] ; then
  echo "restart next experiment..."
  this_script="${RUNSCRIPTDIR}/${job_name}"
  echo 'this_script: ' "$this_script"
  touch ${restartSemaphoreFilename}
  cd ${RUNSCRIPTDIR}
  ${submit} $this_script
else
  [[ -f ${restartSemaphoreFilename} ]] && rm ${restartSemaphoreFilename}
fi

#-----------------------------------------------------------------------------
# automatic call/submission of post processing if available
if [ "x${autoPostProcessing}" = "xtrue" ]; then
  # check if there is a postprocessing is available
  cd ${RUNSCRIPTDIR}
  targetPostProcessingScript="./post.${EXPNAME}.run"
  [[ -x $targetPostProcessingScript ]] && ${submit} ${targetPostProcessingScript}
  cd -
fi

#-----------------------------------------------------------------------------
# check if we test the restart mechanism
get_last_1_restart()
{
  model_restart_param=$1
  restart_list=$(ls *restart_*${model_restart_param}*_*T*Z.nc)
  
  last_restart=""
  last_1_restart=""  
  for restart_file in $restart_list
  do
    last_1_restart=$last_restart
    last_restart=$restart_file

    echo $restart_file $last_restart $last_1_restart
  done  
}


restart_atmo_from=""
restart_ocean_from=""
if [ x$test_restart = "xtrue" ] ; then
  # follows a restart run in the same script
  # set up the restart parameters
  restart_from_folder=${EXPNAME}
  # get the previous from last rstart file for atmo
  get_last_1_restart "atm"
  if [ x$last_1_restart != x ] ; then
    restart_atmo_from=$last_1_restart
  fi
  get_last_1_restart "oce"
  if [ x$last_1_restart != x ] ; then
    restart_ocean_from=$last_1_restart
  fi
  
  EXPNAME=${EXPNAME}_restart
  test_restart="false"
fi

#-----------------------------------------------------------------------------

cd $RUNSCRIPTDIR

#-----------------------------------------------------------------------------

	
# exit 0
#
# vim:ft=sh
#-----------------------------------------------------------------------------

