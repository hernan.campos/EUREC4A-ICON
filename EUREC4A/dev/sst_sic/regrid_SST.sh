#!/bin/bash
module load cdo
# Grep CellArea
cdo -P 8 selname,cell_area ../grids/Testdomain_land_DOM01_DOM01.nc ../grids/Testdomain_land_DOM01_DOM01_CellArea.nc
cdo -P 8 selname,cell_area ../grids/Testdomain_land_DOM01_DOM02.nc ../grids/Testdomain_land_DOM01_DOM02_CellArea.nc
cdo -P 8 selname,cell_area ../grids/Testdomain_land_DOM01_DOM03.nc ../grids/Testdomain_land_DOM01_DOM03_CellArea.nc

cdo -f nc4 remapbil,../grids/Testdomain_land_DOM01_DOM01_CellArea.nc -setgridtype,regular -setmisstonn data/sst_sic.nc data/sst_sic_DOM01.nc
cdo -f nc4 remapbil,../grids/Testdomain_land_DOM01_DOM02_CellArea.nc -setgridtype,regular -setmisstonn data/sst_sic.nc data/sst_sic_DOM02.nc
cdo -f nc4 remapbil,../grids/Testdomain_land_DOM01_DOM03_CellArea.nc -setgridtype,regular -setmisstonn data/sst_sic.nc data/sst_sic_DOM03.nc

