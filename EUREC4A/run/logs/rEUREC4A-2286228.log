-----------------------
 Version of experiment
-----------------------
v0.0.1-329-g3207099-dirty
Keep in mind that this might not be correct in case the script has been modified after submission
-----------------------
 full script print out
-----------------------
#!/bin/bash
# vim:ft=sh
#=============================================================================
# mistral batch job parameters
#-----------------------------------------------------------------------------
#SBATCH --account=mh1126
#SBATCH --job-name=rEUREC4A
#SBATCH --partition=compute
#SBATCH --nodes=128
#SBATCH --threads-per-core=2
#SBATCH --mem=0
#SBATCH --chdir=/work/mh0926/m300872/eureca_icon/EUREC4A/run
#SBATCH --output=logs/%x-%j.log
#SBATCH --time=08:00:00
#SBATCH --mail-user=hernan.campos@mpimet.mpg.de
#SBATCH --mail-type=ALL
#=============================================================================

echo "-----------------------"
echo " Version of experiment"
echo "-----------------------"
git -C $(pwd)/../ describe --always --dirty # add experiment git hash to log file
echo "Keep in mind that this might not be correct in case the script has been modified after submission"
echo "-----------------------"
echo " full script print out"
echo "-----------------------"
cat $0
echo "-----------------------"
echo " START executing script "
echo "-----------------------"
set -x
. ./add_run_routines  # Includes helper functions like warning/print_linked_files/...

#-----------------------------------------------------------------------------
# no idea what this is:
# something with I/O? (https://portia.astrophysik.uni-kiel.de/fosite/namespacefileio__binary__mod.html)
export F_NORCW=65535
ulimit -s $((4 * 1024 * 1024))
ulimit -c 0
#-----------------------------------------------------------------------------

export EXPNAME="$SLURM_JOB_NAME"
# absolute paths of directories
EXPDIR=${BASE_DIR}/experiments/${EXPNAME}
if [ ! -d ${EXPDIR} ] ;  then
  mkdir -p ${EXPDIR}
fi
check_error $? "${EXPDIR} does not exist?"

BASE_DIR=$(dirname $(pwd))

# script_name is used for creating the *.final_status and is the runscript name used in the restart
script_name=$(basename $0)
# save a copy of this script, redundant, but convenient.
cp $BASE_DIR/$script_name $EXPDIR

# ANCILLARY contains three netcdf files with lookup tables
ANCILLARY_DIR=/work/mh0010/m300408/DVC-test/icon-aes/data
# BC contains boundary conditions (sst,initial field, boundaries) and grids
BC_DIR=/work/mh0926/m300872/unchanged

# # This is an attempt to compile by myself:
# ICON_BASE_PATH=/work/mh0926/m300872/sim/icon-aes/build_gcc
# export MODEL=${ICON_BASE_PATH}/bin/icon  
# # This is the original binary from Hauke Schulz
# ICON_BASE_PATH=/work/mh0010/m300408/DVC-test/icon-aes
# export MODEL=${ICON_BASE_PATH}/bin/icon_ba5ebcf0e

# recompile of a copy of Haukes directory
ICON_BASE_PATH=/work/mh0926/m300872/icon_haukestate
export MODEL=${ICON_BASE_PATH}/bin/icon-e8171c42bc
ICON_BASE_PATH=/work/mh0926/m300872/icon_julehelp
export MODEL=${ICON_BASE_PATH}/bin/icon

#-----------------------------------------------------------------------------
run_startdate="2020-01-09T10:00:00Z"
output_startdate_DOM01=${run_startdate}
output_startdate_DOM02="2020-01-09T16:00:00Z"
#placeholderDOM03="2020-02-01T06:05:00Z"
run_enddate="2020-02-19T10:00:00Z"
run_enddate="2020-01-09T11:00:00Z"
run_enddate="2020-01-09T10:10:00Z"

#-----------------------------------------------------------------------------
# target parameters
# ----------------------------

echo "base_dir $BASE_DIR"
echo "script_name $script_name"
echo "SLURM JOB ID $SLURM_JOBID"
echo "SLURM JOB ID $SLURM_JOBID"
echo 'logs/%x-%j.log'
echo "logs/$SLURM_JOB_NAME-$SLURM_JOBID.log"
exit 4

# OpenMP settings
export OMP_NUM_THREADS=4
export ICON_THREADS=$OMP_NUM_THREADS
export OMP_SCHEDULE="guided"
export OMP_DYNAMIC="false"
export OMP_STACKSIZE=500M

export HDF5_USE_FILE_LOCKING=FALSE

export MALLOC_TRIM_THRESHOLD_="-1"

export KMP_AFFINITY="granularity=fine,scatter"
export KMP_LIBRARY="turnaround"

export MKL_DEBUG_CPU_TYPE=5
export MKL_ENABLE_INSTRUCTIONS=AVX2

export OMPI_MCA_btl=self
export OMPI_MCA_coll="^hcoll,ml"
#export OMPI_MCA_coll_hcoll_enable="0"
export OMPI_MCA_io="romio321"
export OMPI_MCA_osc="pt2pt"
export OMPI_MCA_pml="ucx"

#export HCOLL_ENABLE_MCAST_ALL="1"
#export HCOLL_MAIN_IB=mlx5_0:1

export UCX_IB_ADDR_TYPE=ib_global
export UCX_NET_DEVICES=mlx5_0:1
export UCX_TLS=mm,cma,dc_mlx5,dc_x,self
export UCX_UNIFIED_MODE=y
export UCX_HANDLE_ERROS=bt

# load modules
module purge
module use /sw/intel/oneapi/compiler/2021.3.0/modulefiles /sw/intel/oneapi/tbb/2021.3.0/modulefiles /sw/intel/oneapi/mkl/2021.3.0/modulefiles
module load compiler mkl
module load compiler-rt


#-----------------------------------------------------------------------------
# MPI variables
# ----------------------------
no_of_nodes=${SLURM_JOB_NUM_NODES:=1}
mpi_procs_pernode=$((128 / OMP_NUM_THREADS))
((mpi_total_procs=no_of_nodes * mpi_procs_pernode))

mask="0xf,0xf0000,0xf00000000,0xf000000000000,0xf0000000000000000,0xf00000000000000000000,0xf000000000000000000000000,0xf0000000000000000000000000000,0xf0,0xf00000,0xf000000000,0xf0000000000000,0xf00000000000000000,0xf000000000000000000000,0xf0000000000000000000000000,0xf00000000000000000000000000000,0xf00,0xf000000,0xf0000000000,0xf00000000000000,0xf000000000000000000,0xf0000000000000000000000,0xf00000000000000000000000000,0xf000000000000000000000000000000,0xf000,0xf0000000,0xf00000000000,0xf000000000000000,0xf0000000000000000000,0xf00000000000000000000000,0xf000000000000000000000000000,0xf0000000000000000000000000000000"

START="srun -l --kill-on-bad-exit=1 --nodes=${SLURM_JOB_NUM_NODES:-1} --cpu_bind=v,mask_cpu=$mask --distribution=block:block --ntasks=${mpi_total_procs} --propagate=STACK,CORE"

# determine architecture
arch=x86_64-unknown-linux-gnu

if [ -a  /etc/profile ] ; then
. /etc/profile
# Debugging
#loadmodule="arm-forge/19.1.4 $loadmodule"
#module load arm-forge
#START="ddt --connect $START"
fi
set +x


cd ${EXPDIR}
# -----------------------------------------------------------------------------
# RESTART
# -----------------------------------------------------------------------------
# check if this is a restart or not
restartSemaphoreFilename='isRestartRun.sem'             # name of dummy semaphore file
restart=.FALSE.                                         # restart variable is initially false
if [ -f ${BASE_DIR}/run/${restartSemaphoreFilename} ]; then             # if the file "isRestartRun.sem" exists, then set restart to true
  restart=.TRUE.
  previous_exp_folder=`cat ${BASE_DIR}/run/restart_location.txt`        # get previous experiment
  rm ${BASE_DIR}/run/multifile_restart_atm.mfr
  echo "Linking to restart file of last job"
  ln -s ${previous_exp_folder}/multifile_restart_atm.mfr ${BASE_DIR}/run/multifile_restart_atm.mfr
  echo "Linkage done"
fi
echo This is a restart: ${restart}


# -----------------------------------------------------------------------------
# LINKING FILES
# -----------------------------------------------------------------------------

ANCILLARY_DIR=/work/mh0010/m300408/DVC-test/icon-aes/data
EXTPAR_PATH=${BC_DIR}/extpar
GRID_DIR=${BC_DIR}/grids
INIT_DIR=${BC_DIR}/initc/$(TZ=UTC date '+%Y%m%d' -d $run_startdate )
LATBC_PATH=${BC_DIR}/latbc
SSTSIC_PATH=${BC_DIR}/sst_sic/data

# external parameter 
add_link_file ${EXTPAR_PATH}/external-parameter-icon-EUREC4A-PR1250m-DOM01-tiles.nc  extpar_grid_dynamics_1.nc
add_link_file ${EXTPAR_PATH}/external-parameter-icon-EUREC4A-PR1250m-DOM02-tiles.nc  extpar_grid_dynamics_2.nc
add_link_file ${EXTPAR_PATH}/external-parameter-icon-EUREC4A-PR1250m-DOM03-tiles.nc  extpar_grid_dynamics_3.nc

# Radiation
add_link_file ${ANCILLARY_DIR}/rrtmg_lw.nc                 ./
add_link_file ${ANCILLARY_DIR}/ECHAM6_CldOptProps.nc       ./
add_link_file ${ANCILLARY_DIR}/dmin_wetgrowth_lookup.nc    ./

# Grids
LATBC_GRID=${BC_DIR}/latbc/lateral_boundary_DOM01.grid.nc
add_link_file ${GRID_DIR}/EUREC4A_PR1250m_DOM01.nc    ./grid_dynamics_1.nc
add_link_file ${GRID_DIR}/EUREC4A_PR1250m_DOM02.nc    ./grid_dynamics_2.nc
add_link_file ${GRID_DIR}/EUREC4A_PR1250m_DOM03.nc    ./grid_dynamics_3.nc

## Boundary Conditions
# Initial data
#add_link_file ${INIT_DIR}/initc_EUREC4A_PR1250m_DOM01_${init_date}.nc dwdFG_R2B12_DOM01.nc
add_link_file ${INIT_DIR}/initc_EUREC4A_PR1250m_DOM01_$(TZ=UTC date '+%Y%m%d%H' -d $run_startdate).nc dwdFG_R2B12_DOM01.nc

# Lateral boundary conditions
LATBC_FILENAME='<y><m><d>/latbc_EUREC4A_PR1250m_DOM01_<y><m><d><h>.nc'

# Sea surface temperature/ Sea ice cover conditions
add_link_file ${SSTSIC_PATH}/sst_sic_DOM01.nc ./SSTSIC_grid_dynamics_1.nc
add_link_file ${SSTSIC_PATH}/sst_sic_DOM02.nc ./SSTSIC_grid_dynamics_2.nc
add_link_file ${SSTSIC_PATH}/sst_sic_DOM03.nc ./SSTSIC_grid_dynamics_3.nc


# -----------------------------------------------------------------------------
# NAMELISTS
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# global namelist settings

# the namelist filename
atmo_namelist=NAMELIST_${EXPNAME}

# global timing
nhours_restart=7.01
dt_restart=`printf %.0f $(echo "${nhours_restart} * 60 * 60" | bc -l)`  # output of checkpoint and restart
nhours_checkpoint=2.5
dt_checkpoint=`printf %.0f $(echo "${nhours_checkpoint} * 60 * 60" | bc -l)`  # output of checkpoint only (dt_checkpoint < 2.5 hours not allowed in combination with divdamp_order = 24)

# relative start times of nests and meteograms
start_times="0.,21600."  #,9999999"

# the grid parameters
dynamics_grid_filenames="grid_dynamics_1.nc,grid_dynamics_2.nc"  #,grid_dynamics_3.nc"
dynamics_parent_grid_ids="0,1"  #,2"
#radiation_grid_filenames="grid_radiation_1.nc"  # only for coarsest grid

# -----------------------------------------------------------------------------
# ICON master namelist
cat > icon_master.namelist << EOF
&master_nml
 lrestart                     = ${restart}
 lrestart_write_last          = .true.
/
&master_time_control_nml
 experimentStartDate = "$run_startdate"
 experimentStopDate  = "$run_enddate"
 calendar            = 1
/
&time_nml
 ini_datetime_string          = "$run_startdate"
 dt_restart                   = $dt_restart  ! checkpoints are defined in io_nml
/
&master_model_nml
  model_type                  = 1
  model_name                  = "ATMO"
  model_namelist_filename     = "$atmo_namelist"
  model_min_rank              = 1
  model_max_rank              = 65536
  model_inc_rank              = 1
/
EOF

# -----------------------------------------------------------------------------
# ICON model namelist

cat > ${atmo_namelist} << EOF
!
&parallel_nml
 nproma         = 32
 p_test_run     = .false.
 l_test_openmp  = .true.  ! only effective if p_test_run == .true.
 l_log_checks   = .false.
 num_io_procs   = 4  ! FESSTVAL:1; NARVAL:5; HALO:default
 num_prefetch_proc = 1
/
&grid_nml
 ! cell_type is not used = 3            ! triangular cells
 dynamics_grid_filename  = ${dynamics_grid_filenames}
 dynamics_parent_grid_id = ${dynamics_parent_grid_ids}
 !radiation_grid_filename = ${radiation_grid_filenames}
 lfeedback               = .false.
 l_limited_area          = .true.
 start_time              = ${start_times}
 lredgrid_phys           = .false.
/
&initicon_nml
 init_mode              =  7 !4=cosmo, 2=ifs, 3=combined
 dwdfg_filename         = 'dwdFG_R2B12_DOM<idom>.nc'
 lread_ana              = .false.
 ltile_init             = .true.
 ltile_coldstart        = .true.
/
&limarea_nml
 itype_latbc     = 1
 dtime_latbc     = 3600.  ! Time difference between latbc forcing (seconds)
 latbc_boundary_grid = '${LATBC_GRID}'
 latbc_path      = '${LATBC_PATH}'
 latbc_filename  = '${LATBC_FILENAME}'
 init_latbc_from_fg = .true.
/
&io_nml
 itype_pres_msl         = 5
 dt_checkpoint          = ${dt_checkpoint}
 lmask_boundary         = .true.
 lkeep_in_sync          = .false.
 restart_file_type      = 5
 restart_write_mode     = 'joint procs multifile'
/
&run_nml
 num_lev     = 150
 lvert_nest  = .false.
 dtime       = 6            ! why only one value and not one for each nest?
 ldynamics   = .TRUE.       ! dynamics
 ltestcase   = .FALSE.      ! testcase
 ltransport  = .TRUE.       ! transport (but ntracer is 0 by default)
 ntracer     = 5
 iforcing    = 3            ! 0: no forcing (default), 3: NWP forcing
 ltimer      = .true.       !
 timers_level = 10          !
 profiling_output = 1       !
 msg_level   = 9            ! detailed report during integration
 output      = 'nml','totint'
 check_uuid_gracefully = .True.
/
&dynamics_nml
 iequations  = 3
 lcoriolis   = .TRUE.
 idiv_method     = 1
 divavg_cntrwgt  = 0.50
/
&diffusion_nml
 hdiff_order      = 5
 lhdiff_vn        = .TRUE.
 lhdiff_temp      = .TRUE.
 lhdiff_w         = .TRUE.
 hdiff_efdt_ratio = 24       ! recommendation is 30, default is 36
 hdiff_smag_fac   = 0.025    ! default is 0.015
 lsmag_3d         = .FALSE.  ! recommended for mesh size < 1km IF inwp_turb != 5 scheme not used
/
&nwp_phy_nml
inwp_gscp       = 4,4,4        ! 4 for two moment
mu_rain         = 0.5        ! shape parameter in gamma distribution for rain
inwp_convection = 0,0,0
inwp_radiation  = 1,1,1
inwp_cldcover   = 5,5,5        ! 1: diagnostic cloud cover (FESSTVAL); 5: grid scale clouds (NARVAL, HALO)
inwp_turb       = 5,5,5
inwp_satad      = 1,1,1
inwp_sso        = 0,0,0        ! subgrid scale orographic drag 0: none, 1: Lott and Miller scheme (default)
inwp_gwd        = 0,0,0        ! non-orographic gravity wave drag 0: none, 1: Orr-Ern-Bechtold-scheme (default)
inwp_surface    = 1,1,1        ! surface scheme 0: none, 1: TERRA (default)
latm_above_top  = .true.,.true.,.true.
efdt_min_raylfric = 7200.    ! minimum e-folding time of Rayleigh friction (effective for u > 250 m/s)
itype_z0         = 2
dt_rad           = 600.,600.,600.  ! default: 1800s; radiation timestepping; must be multple of dt_conv
dt_gwd           = 300.,300.,300.  ! default: 1200s
dt_sso           = 300.,300.,300.  ! default: 1200s
dt_conv          = 300.,300.,300.  ! default: 600s
icalc_reff = 100,100,100
icpl_rad_reff = 1,1,1
/
&les_nml
 isrfc_type        = 1  !1=TERRA,2=Fixed flux, 5=fixed SST, 3=fixed bflux
 ldiag_les_out     = .FALSE.
 les_metric        = .TRUE.
/
&lnd_nml
ntiles   = 1
lmulti_snow = .false.
lsnowtile = .false.
frlnd_thrhld = 0.5
frlake_thrhld = 0.5
frsea_thrhld = 0.5
lseaice   = .false.
llake     = .false.
itype_lndtbl   = 3
itype_heatcond = 3
sstice_mode = 6
sst_td_filename = "<path>SSTSIC_<gridfile>"
ci_td_filename = "<path>SSTSIC_<gridfile>"
/
&radiation_nml
 irad_o3 = 7
 irad_aero = 6
 vmr_co2   = 390.e-06  ! values representative for 2012
 vmr_ch4   = 1800.e-09
 vmr_n2o   = 322.0e-09
 vmr_o2    = 0.20946
 vmr_cfc11 = 240.e-12
 vmr_cfc12 = 532.e-12
 albedo_type = 2
/
&nonhydrostatic_nml
 iadv_rhotheta    =  2      ! default (2)
 ivctype          =  2      ! default (2)
 itime_scheme     =  4      ! default (4)
 exner_expol      =  0.333  ! default (1./3.)
 vwind_offctr     =  0.25   ! default (0.15); NARVAL: 0.25
 damp_height      =  15000. ! default (45000); NARVAL: 25000.; HALO: 15000; FESSTVAL: 12250
 rayleigh_coeff   =  0.25   ! default (0.05; higher values are recommended for R2B6 and finer)
 lhdiff_rcf       = .true.  ! default (true)
 divdamp_fac      =  0.004  ! default (0.0025); Scaling factor of divergence damping
 divdamp_order    =  24     ! default (4); 24: does not allow checkpointing/restarting earlier than 2.5h of integration
 divdamp_type     =  32     ! default (3); NARVAL: 32
 l_open_ubc       = .true.
 igradp_method    =  3      ! default (3)
 l_zdiffu_t       = .true.  ! default (true)
 thslp_zdiffu     =  0.02   ! default (0.025)
 thhgtd_zdiffu    =  125.   ! default (200)
 htop_moist_proc  =  15500. ! default (22500); NARVAL: 22500
 hbot_qvsubstep   =  15500. ! default (22500); NARVAL: 19000
/
&sleve_nml
 min_lay_thckn   = 20.     ! default (50); NARVAL, FESSTVAL, HALO: 20
 top_height      = 21000.  ! default (23500); NARVAL: 30000; FESSTVAL: 22000; HALO: 21000
 stretch_fac     = 0.9     ! default (1); NARVAL: 0.9; FESSTVAL: 0.65
 decay_scale_1   = 4000.   ! default (4000)
 decay_scale_2   = 2500.   ! default (2500)
 decay_exp       = 1.2     ! default (1.2)
 flat_height     = 16000.  ! default (16000)
/
&turbdiff_nml !!! Daniel Dynmod setup
  tkhmin                      = 0.75  !=MBOld=NARVAL
  tkmmin                      = 0.75  !=MBOld=NARVAL
  tkmmin_strat                = 4     ! NA in MBOld
  pat_len                     = 750.  !=MBOld=NARVAL
  c_diff                      = 0.2   !=MBOld=NARVAL
  rat_sea                     = 7.0   !=8.0 in MBOld
  ltkesso                     = .TRUE.!=MBOld=NARVAL
  frcsmot                     = 0.2   !=MBOld! these 2 switches together apply vertical smoothing of the TKE source terms
  imode_frcsmot               = 2     !=MBOld! in the tropics (only), which reduces the moist bias in the tropical lower troposphere
                                      ! use horizontal shear production terms with 1/SQRT(Ri) scaling to prevent unwanted side effects:
  itype_sher                  = 3     !=MBOld=NARVAL
  ltkeshs                     = .TRUE.!=MBOld=NARVAL
  a_hshr                      = 2.0   !=MBOld=NARVAL
  alpha0                      = 0.0123!=MBOld=NARVAL
  alpha0_max                  = 0.0335!=MBOld=NARVAL
  icldm_turb                  = 1     !=NA in MBOld   !Mode of water cloud representation in turbulence
/
&transport_nml
 ivadv_tracer      = 3,3,3,3,3,3
 itype_hlimit      = 3,4,4,4,4,4
 ihadv_tracer      = 52,2,2,2,2,2
 llsq_svd          = .true.
 beta_fct          = 1.005
/




! RTTOV BEGIN

!&synsat_model_nml
! rttov_coefficient_filename = '/work/mh0010/m300408/DVC-test/rttov/rttov_coef_downloads/rtcoef/rtcoef_goes_16_abi_7gas_ironly.dat'
! rttov_cloud_scattering_coefficient_filename = '/work/mh0010/m300408/DVC-test/rttov/rttov_coef_downloads/sccld/sccldcoef_goes_16_abi_ironly.dat'
! channels     = 1, 2, 3, 4, 5, 6, 7
! patch_id     = 2
! run_start    = ${output_startdate_DOM02}
! run_end      = ${run_enddate}
! run_interval = 'P00DT01H00M00S'
! satellite_subpoint_lat = 0.
! satellite_subpoint_lon = -75.2
! satellite_height = 35.786020e6
!/

!&output_nml
! filetype        = 4
! dom             = 2
! output_start    = ${output_startdate_DOM02}
! output_end      = ${run_enddate}
! output_interval = 'P00DT00H10M00S'
! file_interval   = 'P00DT01H00M00S'
! output_filename = 'EUREC4A_SYNSAT_RTTOV_FORWARD_MODEL'
! filename_format = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
! output_grid     = .true.
! ml_varlist      = 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_1', 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_2', 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_3', 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_4', 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_5', 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_6', 'synsat_rttov_forward_model_2__abi_ir__goes_16__channel_7'
!/

&synsat_model_nml
 rttov_coefficient_filename = '/work/mh0010/m300408/DVC-test/rttov/rttov_coef_downloads/rtcoef/rtcoef_goes_16_abi_7gas_ironly.dat'
 rttov_cloud_scattering_coefficient_filename = '/work/mh0010/m300408/DVC-test/rttov/rttov_coef_downloads/sccld/sccldcoef_goes_16_abi_ironly.dat'
 channels     = 1, 2, 3, 4, 5, 6, 7 ! originally 7, 8, 9, 10, 11, 12, 13
 patch_id     = 1
 run_start    = ${run_startdate}
 run_end      = ${run_enddate}
 run_interval = 'P00DT01H00M00S'
 satellite_subpoint_lat = 0.
 satellite_subpoint_lon = -75.2
 satellite_height = 35.786020e6
/

&output_nml
 filetype        = 4
 dom             = 1
 output_start    = ${output_startdate_DOM01}
 output_end      = ${run_enddate}
 output_interval = 'P00DT00H10M00S'
 file_interval   = 'P00DT01H00M00S'
 output_filename = 'EUREC4A_SYNSAT_RTTOV_FORWARD_MODEL'
 filename_format = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
! output_grid     = .true.
 ml_varlist      = 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_1', 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_2', 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_3', 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_4', 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_5', 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_6', 'synsat_rttov_forward_model_1__abi_ir__goes_16__channel_7'/
/


! ! RTTOV END


&gridref_nml
 grf_intmethod_e  = 5     ! default(6); NARVAL: 5
 grf_scalfbk      = 2     ! default(2)
 grf_tracfbk      = 2     ! default(2)
 denom_diffu_v    = 200.  ! default(200)
/
&interpol_nml
nudge_zone_width  = 10  !-1 create nudge zone in grid
lsq_high_ord      = 3  ! default (3)
!rbf_vec_scale_c   = 0.09, 0.030, 0.010, 0.0040
!rbf_vec_scale_v   = 0.21, 0.070, 0.025, 0.0025
!rbf_vec_scale_e   = 0.45, 0.150, 0.050, 0.0125
rbf_scale_mode_ll = 2 !2=default for automatic calculations
/
&extpar_nml
 itopo          = 1
 n_iter_smooth_topo = 1,1,1,1
 hgtdiff_max_smooth_topo = 750.,750.,750.,750.
 heightdiff_threshold = 1000.,1000.,750.,750.
 extpar_filename = '<path>extpar_<gridfile>'
/
&meteogram_output_nml
 lmeteogram_enabled= .TRUE.,.TRUE.,.TRUE.,.TRUE.
 n0_mtgrm          = ${start_times}           ! meteogram initial time step (0 is first step!)
 ninc_mtgrm        = 10,10,10,10              ! meteogram output interval (steps)
 max_time_stamps   = 100,100,100,100  ! number of output time steps to record in memory before flushing to disk
 zprefix           = 'Meteogram_${SLURM_JOB_ID}_DOM01_', 'Meteogram_${SLURM_JOB_ID}_DOM02_', 'Meteogram_${SLURM_JOB_ID}_DOM03_', 'Meteogram_${SLURM_JOB_ID}_DOM04_'
 append_if_exists  = .True.,.True.,.True.,.True.
 ldistributed      = .false,.false.,.false.,.false.
 stationlist_tot   =  13.3,  -57.717, 'c_center',             ! Lat,Lon
                      12.3,  -57.717, 'c_south',
                      14.3,  -57.717, 'c_north',
                      13.3,  -56.717, 'c_east',
                      13.3,  -58.717, 'c_west',
                      13.16, -59.430, 'BCO',
                      14.75, -51.000, 'NTAS-XVIII'
 /
&output_nml  ! output of constant levels
 dom              = 1  ! master output_nml (-1)
 output_bounds    = 300.,300.,300.   ! adapt to start_times
 filetype         = 5
 steps_per_file   = 1
 include_last     = .true.
 mode             = 2                 ! (default: 2; absolute time axis; 1: relative time axis)
 output_filename  = 'constants'
 filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
 ml_varlist       = 'z_ifc','gz0','topography_c','fr_land','fr_lake',
/
&output_nml  ! output surface variables
 dom              = 1  ! master output_nml (-1)
 output_start     = "${output_startdate_DOM01}"
 output_end       = "${run_enddate}"
 output_interval  = "PT05M"
 file_interval    = "P00DT00H05M00S"
 include_last     = .FALSE.
 mode             = 2                 ! (default: 2; absolute time axis; 1: relative time axis)
 output_filename  = 'surface'
 filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
 ml_varlist       = 'u_10m','v_10m','rh_2m','t_2m','qv_2m','t_seasfc','shfl_s','lhfl_s','tqv_dia','tqc_dia','tqi_dia','rain_gsp_rate','tot_pr
ec','clct','pres_sfc'
/
&output_nml  ! output 3D variables except reff
 dom              = -1  ! master output_nml (-1)
 output_start     = "2020-01-09T12:00:00Z"
 output_end       = "${run_enddate}"
 output_interval  = "P00DT03H00M00S"
 file_interval    = "P00DT01H00M00S"
 include_last     = .FALSE.
 mode             = 2  ! (default: 2; absolute time axis; 1: relative time axis)
 output_filename  = '3D'
 filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
 ml_varlist       = 'u','v','w','temp','pres','theta_v','rho','qv','qc','qr','cloud_num'
 m_levels         = "83...(nlev)"
/
&output_nml  ! output radiation
 dom              = 1  ! master output_nml (-1)
 output_start     = "${output_startdate_DOM01}"
 output_end       = "${run_enddate}"
 output_interval  = "PT10M"
 file_interval    = "PT01H"
 include_last     = .FALSE.
 mode             = 2  ! (default: 2; absolute time axis; 1: relative time axis)
 output_filename  = 'radiation'
 filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
 ml_varlist       = 'sou_t','sob_t','sod_t','thb_t','ddt_temp_radsw','ddt_temp_radlw'  ! 'swflx_dn_clr','swflx_dn','swflx_up_clr','swflx_up',
'lwflx_dn_clr','lwflx_dn','lwflx_up_clr','lwflx_up',
 m_levels         = "83...(nlev)"
/
&output_nml  ! output 3D reff
 dom              = 1  ! master output_nml (-1)
 output_start     = "2020-01-09T12:00:00Z"
 output_end       = "${run_enddate}"
 output_interval  = "P00DT03H"
 file_interval    = "P00DT01H"
 include_last     = .FALSE.
 mode             = 2  ! (default: 2; absolute time axis; 1: relative time axis)
 output_filename  = 'reff'
 filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
 ml_varlist       = 'reff_qc'
 m_levels         = "83...(nlev)"
/
&output_nml  ! output surface variables
 dom              = 2  ! master output_nml (-1)
 output_start     = "${output_startdate_DOM02}"
 output_end       = "${run_enddate}"
 output_interval  = "PT05M"
 file_interval    = "P00DT00H10M00S"
 include_last     = .FALSE.
 mode             = 2                 ! (default: 2; absolute time axis; 1: relative time axis)
 output_filename  = 'surface'
 filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
 ml_varlist       = 'u_10m','v_10m','rh_2m','t_2m','qv_2m','t_seasfc','shfl_s','lhfl_s','tqv_dia','tqc_dia','tqi_dia','rain_gsp_rate','tot_pr
ec','clct','pres_sfc'
/
&output_nml  ! output radiation
 dom              = 2  ! master output_nml (-1)
 output_start     = "${output_startdate_DOM02}"
 output_end       = "${run_enddate}"
 output_interval  = "PT10M"
 file_interval    = "PT01H"
 include_last     = .FALSE.
 mode             = 2  ! (default: 2; absolute time axis; 1: relative time axis)
 output_filename  = 'radiation'
 filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
 ml_varlist       = 'sou_t','sob_t','sod_t','thb_t','ddt_temp_radsw','ddt_temp_radlw'  ! 'swflx_dn_clr','swflx_dn','swflx_up_clr','swflx_up',
'lwflx_dn_clr','lwflx_dn','lwflx_up_clr','lwflx_up',
 m_levels         = "83...(nlev)"
/
&output_nml  ! output 3D reff
 dom              = 2  ! master output_nml (-1)
 output_start     = "2020-01-09T12:00:00Z"
 output_end       = "${run_enddate}"
 output_interval  = "PT03H"
 file_interval    = "PT01H"
 include_last     = .FALSE.
 mode             = 2  ! (default: 2; absolute time axis; 1: relative time axis)
 output_filename  = 'reff'
 filename_format  = "${EXPNAME}_DOM<physdom>_<output_filename>_<datetime2>"
 ml_varlist       = 'reff_qc'
 m_levels         = "83...(nlev)"
/
EOF


#=============================================================================
#
# This section of the run script prepares and starts the model integration. 
#
# bindir and START must be defined as environment variables or
# they must be substituted with appropriate values.
#
# Marco Giorgetta, MPI-M, 2010-04-21
#
#-----------------------------------------------------------------------------
final_status_file=${BASE_DIR}/run/${script_name}.final_status
rm -f ${final_status_file}
#-----------------------------------------------------------------------------
# set some default values and derive some run parameters
restart=${restart:=".false."}
restartSemaphoreFilename='isRestartRunDOM03.sem'
#AUTOMATIC_RESTART_SETUP:
if [ -f ${BASE_DIR}/run/${restartSemaphoreFilename} ]; then
  restart=.true.
  #  do not delete switch-file, to enable restart after unintended abort
  #[[ -f ${restartSemaphoreFilename} ]] && rm ${restartSemaphoreFilename}
fi
#END AUTOMATIC_RESTART_SETUP
#
# wait 5min to let GPFS finish the write operations
#if [ "x$restart" != 'x.false.' -a "x$submit" != 'x' ]; then
#  if [ x$(df -T ${EXPDIR} | cut -d ' ' -f 2) = gpfs ]; then
#    sleep 10;
#  fi
#fi

#-----------------------------------------------------------------------------
# print_required_files
copy_required_files
link_required_files


#-----------------------------------------------------------------------------
# get restart files

#-----------------------------------------------------------------------------
#  get model
#
ls -l ${MODEL}
check_error $? "${MODEL} does not exist?"
#-----------------------------------------------------------------------------

cat > icon_mpmd.conf <<EOF
#
0 numactl --interleave=0-3 -- ${MODEL}
96 numactl --interleave=4-7 -- ${MODEL}
* numactl --localalloc -- ${MODEL}
#
EOF


#-----------------------------------------------------------------------------
# (re)start experiment
basedir=${BASE_DIR}
rm -f finish.status
date
#${START} ${MODEL} # > out.txt 2>&1
${START} --multi-prog icon_mpmd.conf
date

if [ -r finish.status ] ; then
  check_final_status 0 "${START} ${MODEL}"    # ATTENTION: not defined!
else
  check_final_status -1 "${START} ${MODEL}"
fi
#
#-----------------------------------------------------------------------------
#
finish_status=`cat finish.status`
echo $finish_status
echo "============================"
echo "Script run successfully: $finish_status"
echo "============================"

#db# copy this script and the log file to output directory

#-----------------------------------------------------------------------------
# check if we have to restart, ie resubmit
#   Note: this is a different mechanism from checking the restart
if [ $finish_status = "RESTART" ] ; then
  echo "restart next experiment..."
  this_script="${BASE_DIR}/run/${script_name}"
  echo 'this_script: ' "$this_script"
  touch ${BASE_DIR}/run/${restartSemaphoreFilename}
  echo "${EXPDIR}" > ${BASE_DIR}/run/restart_location.txt
  sbatch $this_script
else
  [[ -f ${BASE_DIR}/run/${restartSemaphoreFilename} ]] && rm ${BASE_DIR}/run/${restartSemaphoreFilename}
fi


#-----------------------------------------------------------------------------
# save a copy of the log in the experiment folder
cp ${BASE_DIR}/run/logs/$SLURM_JOB_NAME-$SLURM_JOBID.log $EXP_DIR
#-----------------------------------------------------------------------------

	
# exit 0
#
# vim:ft=sh
#-----------------------------------------------------------------------------

-----------------------
 START executing script 
-----------------------
+ . ./add_run_routines
++ number_of_required_files=0
++ number_of_linked_files=0
+ export F_NORCW=65535
+ F_NORCW=65535
+ ulimit -s 4194304
+ ulimit -c 0
+ export EXPNAME=rEUREC4A
+ EXPNAME=rEUREC4A
+ EXPDIR=/experiments/rEUREC4A
+ '[' '!' -d /experiments/rEUREC4A ']'
+ mkdir -p /experiments/rEUREC4A
mkdir: cannot create directory ‘/experiments’: Permission denied
+ check_error 1 '/experiments/rEUREC4A does not exist?'
+ '[' '' = '' ']'
+ STATUS_FILE=/.status.file
+ echo 1
./add_run_routines: line 43: /.status.file: Permission denied
+ echo 1
./add_run_routines: line 44: /run/.status: Permission denied
+ '[' 1 '!=' 0 ']'
+ echo 'QSUBW_ERROR: JOB_%HOSTNAME%_%PID%: RC = '
QSUBW_ERROR: JOB_%HOSTNAME%_%PID%: RC = 
+ echo 'check_error()'
check_error()
+ echo '   ERROR : /experiments/rEUREC4A does not exist?'
   ERROR : /experiments/rEUREC4A does not exist?
+ exit 1

********************************************************************************
*                                                                              *
*  This is the automated job summary provided by DKRZ.                         *
*  If you encounter problems, need assistance or have any suggestion, please   *
*  write an email to                                                           *
*                                                                              *
*  --  support@dkrz.de --                                                      *
*                                                                              *
*                       We hope you enjoyed the DKRZ supercomputer LEVANTE ... *
*
* JobID            : 2286228
* JobName          : rEUREC4A                                          
* Account          : mh1126
* User             : m300872 (40035), mpiscl (32054)                   
* Partition        : compute
* QOS              : normal
* Nodelist         : l[10022-10023,10039-10041,10203,10227-10230, (128)        
* Submit date      : 2022-10-14T19:39:48
* Start time       : 2022-10-14T19:41:49
* End time         : 2022-10-14T19:41:53
* Elapsed time     : 00:00:04 (Timelimit=08:00:00)                     
* Command          : /work/mh0926/m300872/eureca_icon/EUREC4A/run/exp.work.run
* WorkDir          : /work/mh0926/m300872/eureca_icon/EUREC4A/run
*
* StepID | JobName      NodeHours    MaxRSS [Byte] (@task)
* ------------------------------------------------------------------------------
* batch  | batch             0.14
* extern | extern            0.14                       ()
* ------------------------------------------------------------------------------

