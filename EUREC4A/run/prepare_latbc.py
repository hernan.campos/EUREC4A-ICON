#! /work/mh0010/m300408/anaconda3/envs/ICONrun/bin/python
# -*- coding: utf-8 -*-

# Retrieve lateral boundary conditions

#SBATCH --account=mh0010
#SBATCH --job-name=exp.latbc.run
#SBATCH --partition=prepost
#SBATCH --nodes=1
#SBATCH --threads-per-core=2
#SBATCH --output=logs/LOG.exp.latbc.run.%j.o
#SBATCH --error=logs/LOG.exp.latbc.run.%j.o
#SBATCH --exclusive
#SBATCH --time=01:00:00
#SBATCH --mail-user=hauke.schulz@mpimet.mpg.de
#SBATCH --mail-type=ALL

import argparse
import os
import io
import sys
import resource
import shutil
import glob
import re
import errno
import time
import datetime
import subprocess
import fnmatch
import logging
from omegaconf import OmegaConf
import f90nml
import yaml
import numpy as np
import pandas as pd
import xarray as xr

# Create logger
log = logging.getLogger('latbc runscript')
log.setLevel(logging.DEBUG)
# console handler
ch = logging.StreamHandler()
fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
formatter = logging.Formatter(fmt, '%Y-%m-%d %H:%M:%S')
ch.setFormatter(formatter)
log.addHandler(ch)

# Get configuration
def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-c', '--configfile', metavar="latbc_conf.yaml",
                        help='Provide a latbc_conf.yaml configuration file.',
                        required=True)
    args = vars(parser.parse_known_args()[0])

    return args
args = get_args()
cfg = OmegaConf.load(args['configfile'])

try:
    git_module_version = subprocess.check_output(["git", "describe", "--dirty"]).strip().decode("utf-8")
except:
    git_module_version = "--"

log.info(f"Git version: {git_module_version}")

# Current working directory
log.info('Initial working directory ' + os.getcwd())

# Modules
loadmodules = ["intelmpi/2018.0.128", "intel", "gcc/7.1.0", "nco"]
_ = subprocess.run(["module purge"], shell=True)
for module in loadmodules:
    log.debug(f"Load module {module}")
    _ = subprocess.run(' '.join(["module load", module]), shell=True)
loaded_modules = subprocess.run(["module list"], shell=True, capture_output=True)
log.info(loaded_modules.stderr.decode())

# push OpenMP environment variables
os.environ['ICON_THREADS'] = str(cfg.compute.icon_threads)
os.environ['OMP_NUM_THREADS'] = str(cfg.compute.omp_num_threads)
os.environ['OMP_SCHEDULE'] = str(cfg.compute.omp_schedule)
os.environ['OMP_DYNAMIC'] = str(cfg.compute.omp_dynamic)
os.environ['OMP_STACKSIZE'] = str(cfg.compute.omp_stacksize)

mpi_root = "/sw/rhel6-x64/intel/impi/5.0.3.048/lib64"

no_of_nodes = os.environ.get('SLURM_JOB_NUM_NODES', '1')
log.debug(f"Number of nodes: {os.environ['SLURM_JOB_NUM_NODES']}")
mpi_total_procs = int(no_of_nodes) * cfg.compute.mpi_procs_pernode

start_command_args = ["srun",
                      "--kill-on-bad-exit=1",
                      f"--nodes={no_of_nodes}",
                      f"--ntasks-per-node={cfg.compute.mpi_procs_pernode}"
                      ]

def resolve_local_var(var):
    """Resolve local variable
    """
    localy_avail_vars = dict(globals(), **locals())
    if var in localy_avail_vars.keys():
        return localy_avail_vars[var]
    else:
        raise KeyError(f"Variable {var} unknown. Please specify the variable.")


def prepare_namelist(nml_template, nml_output):
    """Prepare namelist

    1. Load namelist template (yaml)
    2. Resolve variables
    3. Write namelist (f90)

    Input
    -----
    nml_template : str
        path to omegaconf and f90nml compatible
        yaml file
    nml_output : str
        filename of output namelist
    """
    try:
        OmegaConf.register_resolver("local", resolve_local_var)
    except AssertionError:
        # resolver already loaded in an earlier call
        pass
    nml_cfg = OmegaConf.load(nml_template)
    # Resolve variables
    yaml_str = OmegaConf.to_yaml(nml_cfg, resolve=True)
    # Convert to f90 namelist
    yaml_dict = yaml.safe_load(yaml_str)
    if os.path.exists(nml_output):
        log.warning("Existing namelist will be overwritten")
        os.remove(nml_output)
    f90nml.Namelist(yaml_dict).write(nml_output)

    return

# Fill variables in namelists
input_grid = cfg.paths.input.input_grid
local_grid = cfg.paths.input.local_grid
output_grid = os.path.join(cfg.paths.output.outdir_auxiliary_grid, cfg.paths.output.auxiliary_grid)
tmp_weight_file = cfg.remapping.tmp_weight_file+'.latbc'
prepare_namelist(cfg.paths.namelists.templates.iconsub, cfg.paths.namelists.real.iconsub)

# Create output directory
if not os.path.exists(cfg.paths.output.outdir_auxiliary_grid):
    os.makedirs(cfg.paths.output.outdir_auxiliary_grid)
else:
    log.warning(f"Directory {cfg.paths.output.outdir_auxiliary_grid} already exists.")

log.info('Start create sparse latbc grid')
binary = cfg.paths.binaries.iconsub
binary_args = ["-vvv", f"--nml {cfg.paths.namelists.real.iconsub}", "2>&1"]

start_command_args_grid = start_command_args.copy()
start_command_args_grid.append(binary)
start_command_args_grid.extend(binary_args)
start_command = ' '.join(start_command_args_grid)
subprocess.call(start_command, shell=True)

log.info('Finish creating latbc grid')
log.info('Start remaping of data onto latbc grid')

log.debug('Delete temporary weight files of previous run.')
tmp_files = glob.glob(tmp_weight_file+'*')
if len(tmp_files) > 0:
    for file in tmp_files:
        log.info(f'Remove temporary weight file {file}')
        os.remove(file)

output_grid = output_grid+'.grid.nc'  # This seems to be added by iconsub...
prepare_namelist(cfg.paths.namelists.templates.iconremapfields_latbc, cfg.paths.namelists.real.iconremapfields_latbc)

dates = pd.date_range(cfg.remapping.dates[0], cfg.remapping.dates[1])
model_timesteps = np.arange(cfg.remapping.model_timesteps[0], cfg.remapping.model_timesteps[1]+1)

log.info("Start remaping of input files")
for date in dates:
    log.info(date.strftime("Currently remapping files of the %Y%m%d"))
    for timestep in model_timesteps:
        infile = cfg.paths.input.datadir_file_fmt.format(timestep=timestep)
        infile = date.strftime(infile)
        infiles = glob.glob(infile)

        if len(infiles) == 0:
           log.warning(f"No file for the format {infile} found. Skipped.")
        elif len(infiles) == 1:
           infile = infiles[0]
           log.info(f"Loadning file {infile}")
        else:
           infile = infiles[0]
           log.warning(f"Several files found for format {infile}! Taking only the first one ({infile})")

        # Determine actual time of file
        ds = xr.open_dataset(infile)
        start_time = pd.Timestamp(ds.time.values[0])
        del ds

        outfile = cfg.paths.output.outdir_latbc_fmt
        outfile = start_time.strftime(outfile)

        log.debug('Create remap namelist from template')
        prepare_namelist(cfg.paths.namelists.templates.iconremap, cfg.paths.namelists.real.iconremap)

        log.info('Start create sparse latbc grid')
        binary = cfg.paths.binaries.remap
        binary_args = ["-vvv",
                       f"--remap_nml {cfg.paths.namelists.real.iconremap}",
                       f"--input_field_nml {cfg.paths.namelists.real.iconremapfields_latbc}",
                       "2>&1"]

        start_command_args_remap = start_command_args.copy()
        start_command_args_remap.append(binary)
        start_command_args_remap.extend(binary_args)
        start_command = ' '.join(start_command_args_remap)
        log.debug(start_time, outfile, infile, start_command)
        if os.path.exists(outfile):
            log.warning(f"LatBc file ({outfile}) already exists and will be skipped.")
            continue
        elif not os.path.exists(os.path.dirname(outfile)):
            log.debug(f"Create directory {os.path.dirname(outfile)}")
            os.makedirs(os.path.dirname(outfile))
        subprocess.call(start_command, shell=True)

        # Include input file name as source attribute in outfile
        command = [f"ncatted -O -a parent_file,global,a,c,{infile} {outfile}"]
        subprocess.call(command, shell=True)

        if cfg.remapping.overwrite_weights:
            tmp_files = glob.glob(tmp_weight_file+'*')
            if len(tmp_files) > 0:
                for file in tmp_files:
                    os.remove(file)

log.info('Clean up')
tmp_files = glob.glob(tmp_weight_file+'*')
tmp_files.extend([
                   "nml.log", cfg.paths.namelists.real.iconsub,
                   cfg.paths.namelists.real.iconremapfields_latbc,
                   cfg.paths.namelists.real.iconremap])
for file in tmp_files:
    if os.path.exists(file):
        log.debug(f"File {file} will be removed.")
        os.remove(file)

