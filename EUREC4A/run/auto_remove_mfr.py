#!/work/mh0010/m300408/anaconda3/bin/python
"""
Script to delete unused and temporary restart files
"""
import os
import shutil
import glob
import datetime as dt
import subprocess
import hashlib
from filelock import Timeout, FileLock

lock = FileLock("auto_remove.py.lock")
if lock.is_locked:
  print("An other instance seems to be running. Abort.")
else:
  try:
   with lock.acquire(timeout=1):
    path_mfr = "/mnt/lustre02/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/experiments/EUREC4A/"
    pattern_mfr_todelete = "multifile_restart_atm_{date}T{hour:02g}{MMSS:s}Z.mfr"

    all_restart_files = sorted(glob.glob(path_mfr+'multifile_restart_atm_*T*Z.mfr'))

    files_to_delete = []
    files_to_archive = []

    latest_mfr = os.readlink(path_mfr+"multifile_restart_atm.mfr")
    latest_date = dt.datetime.strptime(os.path.basename(latest_mfr),"multifile_restart_atm_%Y%m%dT%H%M%SZ.mfr")

    for mfr_file in all_restart_files:
      fn_parts = mfr_file.split("_")
      yyyymmddTHHMMSSZ = fn_parts[-1].split(".")[0]
      date = yyyymmddTHHMMSSZ[0:8]
      hhmmss = yyyymmddTHHMMSSZ[9:15]
      hh = hhmmss[0:2]

      mfr_date = dt.datetime.strptime(os.path.basename(mfr_file),"multifile_restart_atm_%Y%m%dT%H%M%SZ.mfr")
      print(mfr_file, date, hhmmss, hh, hhmmss[2:], mfr_date)
      if mfr_date < latest_date:
        if mfr_date.hour not in [0,6,12]:
          files_to_delete.append(mfr_file)
        elif (mfr_date.hour in [0,6,12]) and ((mfr_date.minute != 0) or (mfr_date.second != 0)):
          print(mfr_file)
          files_to_delete.append(mfr_file)
        else:
          files_to_archive.append(mfr_file) 

    print("Files to delete:")
    print(files_to_delete)
    for file in files_to_delete:
        print("Removing restart file {file}".format(file=file))
        if ".mfr" in file:
            shutil.rmtree(file)

    print("Files to archive:")
    print(files_to_archive)
 
    import yaml
    from yaml.loader import SafeLoader
    if not os.path.exists('archived_files.yaml'):
      os.mknod("archived_files.yaml")
    with open('archived_files.yaml', 'r') as f:
      archived_file_list = yaml.load(f, Loader=SafeLoader)
      if archived_file_list is None:
        archived_file_list = []
      # Queue each file individually to first compress and afterwards to packems with a dependency on the compression
      # module load packems and compresm
      from modulecmd import Modulecmd
      mcmd = Modulecmd(modulecmd="/sw/rhel6-x64/tcl/modules-3.2.10/Modules/3.2.10/bin/modulecmd")
      mcmd.load("packems")
      mcmd.load("compresm")
      #cmd = subprocess.Popen(['/sw/rhel6-x64/tcl/modules-3.2.10/Modules/3.2.10/bin/modulecmd', 'python', 'load', 'compresm'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      #stdout, stderr = cmd.communicate()
      #cmd = subprocess.Popen(['/sw/rhel6-x64/tcl/modules-3.2.10/Modules/3.2.10/bin/modulecmd', 'python', 'load', 'packems'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      #stdout, stderr = cmd.communicate()
      job_ids_compresm = {}
      command_fmt_to_compress = "sbatch -A mh0010 -p prepost -o compress_%x_%j.log -J {md5sum} --exclusive --mail-type=ALL compresm -j 24 {file}"
      command_fmt_to_archive = "sbatch -A mh0010 --dependency=afterok:{jobid_compress} -p prepost,compute,compute2 -o archive_%x_%j.log --exclusive --mail-type=ALL packems_wrapper -j 12 -o eurec4a_restart_{yyyymmddThh} -S mh0010/m300408/experiments/EUREC4A/mfr/{yyyymmddThh} -O by_time {file}/"
      for file in files_to_archive:
         checksum = hashlib.md5(file.encode('utf-8')).hexdigest()[0:7]  # Attetion: this is the checksum of the filename not of its content!
         if (checksum in archived_file_list):
           print("Already archived or being archived")
           continue
         else:
           archived_file_list.append(checksum)
           already_submitted = subprocess.run("squeue | grep {checksum}".format(checksum=checksum), stdout=subprocess.PIPE, shell=True).stdout.decode()
           if already_submitted is '':
             command_to_compress = command_fmt_to_compress.format(file=file, md5sum=checksum).split()
             print(command_to_compress)
             job_ids_compresm[file] = subprocess.run(command_to_compress, check=True, stdout=subprocess.PIPE).stdout.split()[-1].decode()
             mfr_date = dt.datetime.strptime(os.path.basename(file),"multifile_restart_atm_%Y%m%dT%H%M%SZ.mfr")
             command_to_archive = command_fmt_to_archive.format(jobid_compress=job_ids_compresm[file], file=file, yyyymmddThh=mfr_date.strftime("%Y%m%dT%H"))
             print(command_to_archive)
             subprocess.run(command_to_archive.split(), check=True, stdout=subprocess.PIPE).stdout
    with open('archived_files.yaml', 'w') as f:
      yaml.dump(archived_file_list, f)
  except Timeout:
     print("Application already running. Abort")

